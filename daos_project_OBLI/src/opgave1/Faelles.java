package opgave1;

import java.util.Random;

public class Faelles {
	private int global;
	
	
	public Faelles() {
		this.global = 0;
	}
	
	
	public int getTaeller()  {
		return this.global;
	}
	
	public void tagerRandomTid(int max) {
		Random r  = new Random();
		int nyMax = Math.abs(r.nextInt())%max+1;
		int plus = 0;
		int minus = 0;
		for (int i = 0; i < nyMax; i++) {
			plus++;
			for (int l = 0; l < nyMax; l++) {
				minus--;
			}
		}
		
	}
	
	public int getGlobal() {
		return global;
	}
	
	
	public void kritiskSection() {
		int temp;
		temp = global;
		tagerRandomTid(100000);
		global = temp+1;
		
	}
	
	
	
	
	

}
