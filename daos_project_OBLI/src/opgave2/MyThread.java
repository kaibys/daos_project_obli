package opgave2;

public class MyThread extends Thread {

	private Faelles faelles;

	private int thisId;
	private int concurrentId;


	public MyThread(String navn, Faelles faelles, int id, long maxMillis) {
		this.faelles = faelles;
		this.thisId = id;
		super.setName(navn);



	}


	public void run() {


		concurrentId = (thisId +1) % 2;
		faelles.setFlag(true, thisId);
		faelles.setTurn(concurrentId);
		while (faelles.getFlag(concurrentId) &&
				faelles.getTurn() == concurrentId); 
			//kritisk
			for (int j = 0; j < 100; j++) {

				faelles.kritiskSection();
				faelles.tagerRandomTid(10000);

			}
			faelles.setFlag(false, thisId);
			System.out.println(super.getName() + faelles.getGlobal());
		
	}



}
