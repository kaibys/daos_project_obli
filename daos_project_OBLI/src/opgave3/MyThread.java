package opgave3;

public class MyThread extends Thread {
	
	private Faelles faelles;
	
	public MyThread(String navn, Faelles faelles) {
		super.setName(navn);
		this.faelles = faelles;
		
	}
	
	public void run() {
		for (int j = 0; j < 100; j++) {
			faelles.kritiskSection();
			faelles.tagerRandomTid(10000);
		}
		System.out.println(super.getName() + faelles.getGlobal());
	}
	
	
	
}
